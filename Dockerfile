FROM node:15.2.0-alpine3.10

ARG TESTWILA
ENV TESTOUILLE=$TESTWILA
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY . .
RUN npm run build
# EXPOSE 3000
CMD ["npm", "run", "start:prod"]