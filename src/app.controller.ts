import { Controller, Get, Res, Post, HttpService } from '@nestjs/common';
import { AppService } from './app.service';
import { Response } from 'express';
const fetch = require("node-fetch");

@Controller("/")
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  returnValueFromHereThroughGet(@Res() res: Response){
    res.json({ message: 'Psartek ' + process.env.TESTOUILLE });
  }

  @Get("testAPI1")
  returnValueFromHereThroughGette(@Res() res: Response){
    res.json({ message: 'Je viens de l API 1' });
  }

  @Post()
  returnValueFromHereThroughPost(@Res() res: Response){
    res.json({ message: 'Je viens de l API 1' });
  }

  @Get('getTestAPI2')
  async getValueFromAPI2(@Res() res: Response){
    let test = await fetch('service2:3000/testAPI2', { method: 'GET'})
    .then(res => res.json())
    res.json(test)
  }

  @Get('postTestAPI2')
  async postValueFromAPI2(@Res() res: Response){
    let test = await fetch('service2:3000/testAPI2', { method: 'POST'})
    .then(res => res.json())
    res.json(test)
  }
}
